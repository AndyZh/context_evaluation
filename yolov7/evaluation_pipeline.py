import argparse
import glob
import json
from pathlib import Path

import numpy as np
import fiftyone as fo
import pandas as pd
import os
from libia.evaluation.detection import DetectionEvaluator as de
from libia.dataset import Dataset, from_darknet_generic, from_caipy, from_caipy_generic
import seaborn as sns
import matplotlib.pyplot as plt

from detect import detect
from models.experimental import attempt_load


def generate_predictions(opt):
    """ Generate predictions with a yolov7 model
    Returns:
        Dataset: a libia Dataset object with predictions """

    # set detect() parameters
    opt.source = str(opt.dataset/"Images")
    opt.save_conf = opt.save_txt = opt.exist_ok = opt.nosave = True
    opt.view_img = opt.no_trace = opt.augment = opt.classes = opt.agnostic_nms = None

    # generate predictions in yolo format
    detect(opt)

    # get model names
    model = attempt_load(opt.weights)
    names = model.module.names if hasattr(model, 'module') else model.names

    # load predictions into a libia.dataset and reset the image ids to match the groundtruths image_ids
    predictions = from_darknet_generic(images_root=opt.source,
                                       labels_root=Path(opt.project) / f"{opt.name}/labels",
                                       names=names).reset_index()

    # remap predictions if the user provided a .csv remap file
    if opt.pred_remap:
        predictions = predictions.remap_from_csv(opt.pred_remap)

    return predictions


def load_predictions(pred_path: Path, remap_csv: Path = None):
    """ Loads caipyjson predictions and remaps them if a remap .csv is provided,
    then returns a Dataset object for predictions
    Args:
        pred_path (Path): Path to the root directory containing caipyjson prediction files.
        remap_csv (Path): Path to a .csv file to use to remap predictions
    Returns:
        Dataset: a libia Dataset object with predictions """

    predictions = from_caipy_generic(annotations_folder=pred_path,
                                     images_folder=dataset_path / "Images").reset_index()
    # remap predictions if the user provided a .csv remap file
    if remap_csv:
        predictions = predictions.remap_from_csv(remap_csv)

    return predictions


def eval_with_libia(groundtruth: Dataset, predictions: Dataset, out_dir: Path):
    """ Evaluate a model's prediction on an evaluation dataset with libia. Inspired from the official libia tutorial:
     https://gitlab.com/xxii-group/research-and-development/libia/-/blob/dev/docs/notebooks/demo_evaluation.ipynb """

    # create evaluator object
    evaluator = de(groundtruth=groundtruth, predictions=predictions)
    # compute matches between groundtruth and predictions
    evaluator.compute_matches("predictions")

    # compute PR and AP metrics for IoU of 0.5:0.95
    pr, ap = evaluator.compute_precision_recall(
        predictions="predictions",
        ious=np.linspace(0.5, 0.95, 10).round(3),
        index_column=None)
    ap_consolidated = pd.pivot_table(ap, index="category_id", columns="iou_threshold")
    ap_consolidated["mean"] = ap_consolidated["AP"].mean(axis=1)

    # compute precision & recall & f1score for IoU 0.5
    pr50, ap50 = evaluator.compute_precision_recall(
        predictions="predictions",
        ious=0.5,
        index_column="confidence_threshold",
        index_values=np.linspace(0, 1, 101),
        f_scores_betas=(0.5, 1, 2))
    mean_f1 = pr50.groupby("confidence_threshold").mean()
    best_conf_thresh = mean_f1["f1_score"].idxmax()
    best_f1_score = mean_f1.loc[best_conf_thresh]
    best_f1_score_per_class = pr50[pr50.confidence_threshold == best_conf_thresh].set_index("category_id")

    # compute precision & recall & f1score for IoU 0.3
    pr30, ap30 = evaluator.compute_precision_recall(
        predictions="predictions",
        ious=0.3,
        index_column="confidence_threshold",
        index_values=np.linspace(0, 1, 101),
        f_scores_betas=(0.5, 1, 2))

    # export libia evaluation results
    libia_eval_results = {
        "mAP@0.3": ap30["AP"].mean(),
        "mAP@0.5": ap[ap['iou_threshold'] == 0.5]['AP'].mean(),
        "mAP@0.5:0.95": ap['AP'].mean(),
        "precision": best_f1_score['precision'],
        "recall": best_f1_score['recall'],
        "f1_score": best_f1_score['f1_score'],
        "best_confidence_threshold_(f1_score)": best_conf_thresh,
    }
    for category_id, row in ap_consolidated.iterrows():
        libia_eval_results[groundtruth.label_map[category_id]] = {
                "AP@0.5": row["AP"][0.5],
                "AP@0.5:0.95": row["mean"][0],
                "precision": best_f1_score_per_class.loc[category_id]["precision"],
                "recall": best_f1_score_per_class.loc[category_id]["recall"],
                "f1_score": best_f1_score_per_class.loc[category_id]["f1_score"],
                }

    for category_id in groundtruth.label_map.keys():
        pr_persons = pr[pr["category_id"] == category_id]
        sns.relplot(
            data=pr_persons,
            x="recall",
            y="precision",
            hue="iou_threshold",
            kind="line",
            estimator=None,
            sort=False)
        plt.title(f"\n{groundtruth.label_map[category_id]}")
        plt.savefig(out_dir/f"libia_eval_results_{groundtruth.label_map[category_id]}.png", dpi=300, bbox_inches='tight')

    with open(out_dir/"libia_eval_results.json", "w") as jfile:
        json.dump(libia_eval_results, jfile, indent=3)


def eval_with_fiftyone(groundtruths: Dataset, predictions: Dataset, preds_name: str, out_dir:Path):
    """ Evaluate a model's prediction on an evaluation dataset with fiftyone """

    # remove tags and other problematic columns from images to get around a current bug in libia.dataset.to_fiftyone()
    groundtruths.images = groundtruths.images.drop(columns=["tags", "flickr_url", "coco_url", "date_captured",
                                                            "status"],
                                                   errors="ignore")
    
    groundtruths.annotations = groundtruths.annotations.drop(columns=["attributes", "area"],
                                                   errors="ignore")

    # get the fiftyone dataset name (the root directory of the caipyjson dataset)
    fo_dataset_name = groundtruths.dataset_path.parent.name if groundtruths.dataset_path.name == "Images" \
        else groundtruths.dataset_path.name
    # load groundtruths to fiftyone
    # if a dataset already exists in fiftyone with the same name, we load it from fiftyone
    if fo_dataset_name in fo.list_datasets():
        print(f"Warning: The dataset {fo_dataset_name} already exists in fiftyone. It will be loaded from fiftyone")
        fo_dataset = fo.load_dataset(fo_dataset_name)
    # if the dataset does not exist in fiftyone, we load the groundtruths libia dataset into fiftyone
    else:
        fo_dataset = groundtruths.to_fiftyone(dataset_name=fo_dataset_name, annotations_name="groundtruth")
        fo_dataset.persistent = True

    # load predictions to fiftyone
    if fo_dataset.get_field(preds_name):
        raise ValueError(f"The fiftyone dataset {fo_dataset_name} already contains predictions named {preds_name}. "
                         f"Please provide a different --name argument for your run")
    else:
        predictions.to_fiftyone(dataset_name=fo_dataset_name, annotations_name=preds_name)
    # run fiftyone evaluation
    eval_result = fo_dataset.evaluate_detections(pred_field=preds_name,
                                                 gt_field="groundtruth",
                                                 eval_key=f"eval_{preds_name}",
                                                 compute_mAP=True)

    # export fiftyone evaluation results
    fo_eval_results = {"mAP@0.5:0.95": eval_result.mAP()} | eval_result.report()
    for cls in groundtruths.label_map.values():
        fo_eval_results[cls]["AP@0.5:0.95"] = eval_result.mAP([cls])

    with open(out_dir/"fiftyone_eval_results.json", "w") as jfile:
        json.dump(fo_eval_results, jfile, indent=3)


def remove_useless_classes(prediction, gt_label_map):
    """
    Remove classes that are not in ground truth (we want to ignore them) in predictions
    Args:
        prediction: the prediction dataframe
        gt_label_map: the ground truth label map

    Returns:
    """
    classes_to_remove = []
    for ids in prediction.label_map.keys():
        if ids not in gt_label_map.keys():
            classes_to_remove.append(ids)
    prediction = prediction.remove_classes(classes_to_remove)

    return prediction


def remove_objects_classes_not_in_gt(prediction, groundtruth):
    """
    Some images/annotations are from dataset where some classes are not annotated but annotated on other images.
    Example : The pedestrian in UAV dataset are not annotated but in P2C they are. If the detection of pedestrian
    of UAV images are not removed, the evaluation will consider it as a false positive and this may be wrong because
    there are no annotations of pedestrian in UAV.
    We are then going to remove all the classes that are not present in the ground truth on every detection in order to
    no bias the evaluation
    Args:
        prediction: prediction dataframe
        groundtruth: ground truth dataframe
    Returns:
        prediction : prediction dataframe where the classes not in gt has been removed
    """
    unique_image_ids = groundtruth.annotations['image_id'].unique()
    index_to_remove = []
    for image_id in unique_image_ids:
        current_image_annotation_gt = groundtruth.annotations[groundtruth.annotations['image_id'] == image_id]
        current_image_annotation_pred = prediction.annotations[prediction.annotations['image_id'] == image_id]
        image_label_map = current_image_annotation_gt['category_id'].unique()

        for index in current_image_annotation_pred.index:
            pred_id = current_image_annotation_pred.loc[index, :]['category_id']
            if pred_id not in image_label_map:
                index_to_remove.append(index)
    prediction.annotations = prediction.annotations.drop(index_to_remove)
    return prediction


def remove_objects_in_ignore_areas(prediction, groundtruth):
    """
    Remove all the predictions that are in an ignore areas
    Args:
        prediction: prediction dataframe
        groundtruth: groundtruth dataframe
    Returns:
    """
    if -1 not in groundtruth.label_map:  # If no ignore areas in the gt
        return prediction
    
    index_objects_in_ignore_areas = []
    for image_id in groundtruth.annotations["image_id"].unique():
        ignore_areas = []

        for index in groundtruth.annotations[groundtruth.annotations["image_id"] == image_id].index:
            gt_id = groundtruth.annotations.loc[index, :]['category_id']

            if gt_id == -1:
                xmin = groundtruth.annotations.loc[index, :]['box_x_min']
                ymin = groundtruth.annotations.loc[index, :]['box_y_min']
                width = groundtruth.annotations.loc[index, :]['box_width']
                height = groundtruth.annotations.loc[index, :]['box_height']
                ignore_areas.append([xmin, ymin, width, height])
        
        for index in prediction.annotations[prediction.annotations["image_id"] == image_id].index:
            pred_image_id = prediction.annotations.loc[index, :]['image_id']
            if image_id == pred_image_id:
                xmin_pred = prediction.annotations.loc[index, :]['box_x_min']
                ymin_pred = prediction.annotations.loc[index, :]['box_y_min']
                width_pred = prediction.annotations.loc[index, :]['box_width']
                height_pred = prediction.annotations.loc[index, :]['box_height']
                bbox_pred = [xmin_pred, ymin_pred, width_pred, height_pred]
                for ignore_area in ignore_areas:
                    intersection = get_bbox_intersection(bbox_pred, ignore_area)
                    bbox_pred_area = height_pred * width_pred
                    if intersection > 0.5*bbox_pred_area:  # If the intersection of the pred is at least half of the pred bbox
                        index_objects_in_ignore_areas.append(index)  # We ignore this prediction
    prediction.annotations = prediction.annotations.drop(index_objects_in_ignore_areas)
    return prediction


def get_bbox_intersection(bbox1, bbox2):
    """
    Compute the intersection of two bbox [xmin, ymin, width, height]
    Args:
        bbox1: the first bbox
        bbox2: the second bbox
    Returns: The area of the intersection
    """
    x_left = max(bbox1[0], bbox2[0])
    y_top = max(bbox1[1], bbox2[1])
    bbox1_x2 = bbox1[0] + bbox1[2]
    bbox1_y2 = bbox1[1] + bbox1[3]
    bbox2_x2 = bbox2[0] + bbox2[2]
    bbox2_y2 = bbox2[1] + bbox2[3]
    x_right = min(bbox1_x2, bbox2_x2)
    y_bottom = min(bbox1_y2, bbox2_y2)

    if x_right < x_left or y_bottom < y_top:
        return 0

    intersection = (x_right - x_left) * (y_bottom - y_top)

    return intersection


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="An evaluation pipeline that uses libia and fiftyone to evaluate "
                                                 "yolov7 weights or caipyjson predictions on a caipyjson dataset.")
    parser.add_argument('--weights', nargs='+', type=str,  help='model.pt path(s)')
    parser.add_argument('--dataset', type=Path, help='path to caipyjson evaluation dataset')
    parser.add_argument('--predictions', type=Path, help='Path to a directory containing caipyjson predictions')
    parser.add_argument('--pred_remap', type=Path, help='Path to a .csv file to use to remap predictions with libia')
    parser.add_argument('--img-size', type=int, default=640, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.01, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.45, help='IOU threshold for NMS')
    parser.add_argument('--max_nms', type=int, default=30000, help='maximum number of boxes into torchvision.ops.nms()')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--project', help='save results to project/name. If not specified, '
                                          'project will be runs/<dataset root directory name>')
    parser.add_argument('--name', help='save results to project/name. This will also serve as the name of the '
                                       'predictions in the fiftyone dataset. If not specified, will use the name of the'
                                       ' weights file')
    parser.add_argument('--no-fiftyone', action='store_true', help='Do not run an evaluation with fiftyone. '
                                                                   'Only use libia')
    opt = parser.parse_args()

    # if project is not provided by the user, use runs/evaluation/<dataset's root directory name>
    if not opt.project:
        opt.project = str(Path("runs/evaluation")/opt.dataset.stem)
    # if name is not provided by the user, use the weights filename or the predictions root directory name
    if not opt.name:
        opt.name = Path(opt.weights[0]).stem if opt.weights else opt.predictions.stem

    # make sure that the run directory does not exist then create it
    run_dir = Path(opt.project) / opt.name
    if run_dir.exists():
        raise FileExistsError(f"The directory that will contain your predictions already exists: {run_dir} \n"
                              f"Remove the directory or specify a different --name argument for your run")
    else:
        run_dir.mkdir(parents=True)

    # load groundtruths and reset the image ids to match the prediction image_ids
    groundtruth = from_caipy(opt.dataset).reset_index()

    # generate predictions on the evaluation dataset using the provided yolov7 weights
    if not opt.predictions and opt.weights:
        predictions = generate_predictions(opt)
    # or directly load the provided caipyjson predictions
    elif not opt.weights and opt.predictions:
        predictions = load_predictions(opt.predictions, opt.pred_remap)
    else:
        raise ValueError("You need to either provide caipyjson predictions using --predictions or provide yolov7 "
                         "weights using --weights to generate predictions")

    # We remove all the classes that are not in the dataset gt
    predictions = remove_useless_classes(predictions, groundtruth.label_map)

    # We remove all the predictions that are not in the GT of the image
    predictions = remove_objects_classes_not_in_gt(predictions, groundtruth)

    # We remove all the predictions that are in ignore areas
    predictions = remove_objects_in_ignore_areas(predictions, groundtruth)

    # We remove all the GT that are in ignore areas
    groundtruth = remove_objects_in_ignore_areas(groundtruth, groundtruth)

    # We finally remove all the ignore areas from the GT
    groundtruth = groundtruth.remove_classes([-1])

    # run an evaluation with libia
    eval_with_libia(groundtruth, predictions, out_dir=run_dir)

    # Save the predictions in caipy format
    caipy_pred_dir = f"{run_dir}/caipy"
    predictions.to_caipy(caipy_pred_dir, copy_images=False, overwrite_images=False)
    caipy_annotations_dir = f"{caipy_pred_dir}/Annotations"
    new_caipy_annotations_dir = f"{run_dir}/caipy_annotations"
    os.system(f"mv {caipy_annotations_dir} {new_caipy_annotations_dir}")
    os.system(f"rm -r {caipy_pred_dir}")

    # run an evaluation with fiftyone
    if not opt.no_fiftyone:
        eval_with_fiftyone(groundtruth, predictions, preds_name=opt.name, out_dir=run_dir)
