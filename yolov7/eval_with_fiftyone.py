#!/usr/bin/env python3
import sys
sys.path.insert(0, 'context_eval')
from pathlib import Path
from libia.dataset import from_caipy
from evaluation_pipeline import eval_with_fiftyone, from_caipy_generic, remove_objects_in_ignore_areas, eval_with_libia

DATASET_DIRECTORY = "/home/andyzhang/Bureau/TEST_EVAL_FIFTYONE_NIGHT"


def run_fiftyone_evaluation():
    """
    Run the evaluation with fiftyone
    Returns:
    """
    gt_path = f"{DATASET_DIRECTORY}/test_eval_fiftyone_3"
    groundtruth = from_caipy(gt_path).reset_index()
    
    pred_folder = f"{DATASET_DIRECTORY}/rain_synthetic_pred_on_clear_night"
    image_folder = f"{gt_path}/Images"
    predictions = from_caipy_generic(annotations_folder=pred_folder, images_folder=image_folder).reset_index()

    groundtruth = remove_objects_in_ignore_areas(groundtruth, groundtruth) # We remove all the GT that are in ignore areas
    groundtruth = groundtruth.remove_classes([-1]) # Remove the "ignore areas" from annotations

    out_dir = Path(DATASET_DIRECTORY)
    # run an evaluation with libia
    eval_with_fiftyone(groundtruth, predictions, preds_name="2_rain_synthetic_pred_on_clear_night", out_dir=out_dir)


if __name__ == "__main__":
    # Run the evaluation
    run_fiftyone_evaluation()
