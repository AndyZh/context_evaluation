#!/usr/bin/env python3
import os
import glob
from libia.dataset import Dataset, from_caipy, from_caipy_generic
import argparse

DATASET_DIRECTORY = "/home/andyzhang/Bureau/Benchmark_dataset"
os.chdir(DATASET_DIRECTORY)


def remove_image_id(groundtruth, prediction, image_ids):
    """
    Removes from the gt and the prediction, all the images/objets where the image id are in image_ids
    Args:
        groundtruth: the gt dataframe
        prediction: the prediction dataframe
        image_ids: the list of image ids we want to delete image and annotations
    Returns: the gt and prediction where the images and annotations about image_id in image_ids has been deleted
    """
    # Remove all the images
    groundtruth.images = groundtruth.images.drop(image_ids)
    prediction.images = prediction.images.drop(image_ids)

    # Removes all the annotations in gt and prediction that are in the images we removed
    for image_id in image_ids:
        groundtruth.annotations = groundtruth.annotations[groundtruth.annotations['image_id'] != image_id]
        prediction.annotations = prediction.annotations[prediction.annotations['image_id'] != image_id]

    return groundtruth, prediction


def load_obj_prediction(context_path, model):
    """
    This function loads the predictions that where stored in the caipy format after the script all_context_eval_yolov7.py
    Args:
        context_path: the context
        model: the model we want to evaluate (yolov7 or scaled_yolov4)
    Returns: the prediction dataframe containing
    """
    context = context_path.split("/")[-1]
    pred_folder = f"/home/andyzhang/context_evaluation/yolov7/runs/evaluation/{context}/{model}/caipy_annotations"
    image_folder = f"{context_path}/Images"
    predictions = from_caipy_generic(annotations_folder=pred_folder, images_folder=image_folder).reset_index()

    return predictions


def get_real_datasets():
    """
    Get all the directories of the different real contextual dataset
    Returns: The list of the directories
    """
    real_data_directory = f"{DATASET_DIRECTORY}/Real/data"
    #real_data_directory = f"/home/andyzhang/Bureau/preprocessed"
    real_dataset_directory = []
    for folder in glob.glob(f"{real_data_directory}/*"):
        real_dataset_directory.append(folder)
    return real_dataset_directory


def get_synthetic_datasets():
    """
    Get all the directories of the different synthetic contextual dataset
    Returns: The list of the directories
    """
    synthetic_data_directory = f"{DATASET_DIRECTORY}/Synthetic/data"
    synthetic_dataset_directory = []
    for folder in glob.glob(f"{synthetic_data_directory}/*/*"):
        synthetic_dataset_directory.append(folder)
    return synthetic_dataset_directory
    #return []


def get_all_datasets():
    """
    Get all the directories of the different contextual dataset
    Returns: The list of the directories
    """
    real_datasets = get_real_datasets()
    synthetic_dataset = get_synthetic_datasets()
    list_datasets_path = real_datasets + synthetic_dataset
    return list_datasets_path


def init_eval():
    """
    Initialize the evaluation by retrieving the model we want to evaluate when launching the different scripts
    Returns: The model we want to evaluate
    """
    parser = argparse.ArgumentParser(description="An evaluation pipeline that uses libia and fiftyone to evaluate "
                                                 "yolov7 weights or caipyjson predictions on a caipyjson dataset.")
    parser.add_argument('--model', nargs='+', type=str, required=True,
                        help='The model we evaluate : yolov7 or yolov4-p5')
    opt = parser.parse_args()

    model = opt.model[0]

    return model
