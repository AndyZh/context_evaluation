#!/usr/bin/env python3
import os
import sys
sys.path.insert(0, '..')
from pathlib import Path
from libia.dataset import Dataset, from_caipy, from_caipy_generic
from evaluation_pipeline import eval_with_libia
from context_utility_functions import remove_image_id, load_obj_prediction, get_all_datasets, init_eval
import shutil

DATASET_DIRECTORY = "/home/andyzhang/Bureau/Benchmark_dataset"
os.chdir(DATASET_DIRECTORY)


def filter_object_number(groundtruth, prediction, number):
    """
    This functions removes from the gt and the prediction, all the images that doest verify number
    Args:
        groundtruth: gt dataframe
        prediction: prediction dataframe
        number: "less_5", "more_5" or "more_30"
    Returns:
    """
    unique_image_ids = groundtruth.annotations['image_id'].unique()
    image_id_to_remove = []
    for image_id in unique_image_ids:
        current_image_annotation_gt = groundtruth.annotations[groundtruth.annotations['image_id'] == image_id]
        number_of_obj = current_image_annotation_gt.shape[0]
        if number == "less_5":
            if number_of_obj > 5:
                image_id_to_remove.append(image_id)
        elif number == "more_5":
            if number_of_obj <= 5:
                image_id_to_remove.append(image_id)
        else:
            if number_of_obj < 30:
                image_id_to_remove.append(image_id)

    groundtruth, prediction = remove_image_id(groundtruth, prediction, image_id_to_remove)

    return groundtruth, prediction


def run_object_number_evaluation(list_datasets_path, model):
    """
    Run the evaluation for all context, for images with less than 5, more than 5 and more than 30 objects
    Args:
        list_datasets_path: The list of dataset path
        model : the model we evaluate
    Returns:
    """
    for context_path in list_datasets_path:
        for number in ["less_5", "more_5", "more_30"]:
            groundtruth = from_caipy(context_path).reset_index()
            groundtruth = groundtruth.remove_classes([-1])  # Remove the "ignore areas" from annotations

            predictions = load_obj_prediction(context_path, model)

            groundtruth, predictions = filter_object_number(groundtruth, predictions, number)

            # If there is no image with more than 30 objets
            if len(groundtruth) == 0:
                continue

            yolov7_directory = "/home/andyzhang/context_evaluation/yolov7"
            os.chdir(yolov7_directory)

            context = context_path.split("/")[-1]
            output_dir = Path("runs/evaluation") / context / model / number
            if os.path.exists(output_dir):
                shutil.rmtree(output_dir)
            os.mkdir(output_dir)
            # run an evaluation with libia
            eval_with_libia(groundtruth, predictions, out_dir=output_dir)


if __name__ == "__main__":
    model = init_eval()

    # Retrieve all the dataset paths
    all_context_path = get_all_datasets()

    # Run the evaluation
    run_object_number_evaluation(all_context_path, model)
