#!/usr/bin/env python3
import os
import sys
sys.path.insert(0, '..')
from pathlib import Path
from libia.dataset import from_caipy
from evaluation_pipeline import eval_with_libia
from context_utility_functions import get_all_datasets, load_obj_prediction, init_eval

DATASET_DIRECTORY = "/home/andyzhang/Bureau/Benchmark_dataset"
os.chdir(DATASET_DIRECTORY)


def filter_object_size(dataframe, size):
    """
    This function filters the dataframe according to the size of the objects we want to keep
    Args:
        dataframe: The dataframe containing the annotations or predictions
        size: The size "small" or "big"
    Returns: The dataframe containing only small or big objects
    """
    object_to_remove_from_dataframe = []
    for index in dataframe.annotations.index:
        image_id = dataframe.annotations.loc[index, :]['image_id']
        image = dataframe.images.loc[image_id, :]

        # Image area
        image_width = image['width']
        image_height = image['height']
        image_area = image_height * image_width

        # Object area
        object_width = dataframe.annotations.loc[index, :]['box_width']
        object_height = dataframe.annotations.loc[index, :]['box_height']
        object_area = object_width * object_height

        if size == "small":  # We keep small objects
            if object_area > image_area/200:  # If it is a big object, we remove it from the dataframe
                object_to_remove_from_dataframe.append(index)
        else:
            if object_area <= image_area/200:  # If it is a small object, and we want to keep big objects
                object_to_remove_from_dataframe.append(index)

    dataframe.annotations = dataframe.annotations.drop(object_to_remove_from_dataframe)

    return dataframe


def run_object_size_evaluation(list_datasets_path, model):
    """
    Run the evaluation for all context, for small and big objects bbox
    Args:
        list_datasets_path: The list of dataset path
    Returns:
    """
    for context_path in list_datasets_path:
        for size in ["small", "big"]:
            groundtruth = from_caipy(context_path).reset_index()
            groundtruth = groundtruth.remove_classes([-1])  # Remove the "ignore areas" from annotations
            groundtruth = filter_object_size(groundtruth, size)

            predictions = load_obj_prediction(context_path, model)
            predictions = filter_object_size(predictions, size)

            yolov7_directory = "/home/andyzhang/context_evaluation/yolov7"
            os.chdir(yolov7_directory)

            context = context_path.split("/")[-1]
            output_dir = Path("runs/evaluation") / context / model / size
            os.system(f"test -f {str(output_dir)} && rm -r {str(output_dir)}")
            os.mkdir(output_dir)
            # run an evaluation with libia
            eval_with_libia(groundtruth, predictions, out_dir=output_dir)


if __name__ == "__main__":
    model = init_eval()

    # Retrieve all the dataset paths
    all_context_path = get_all_datasets()

    # Run the evaluation
    run_object_size_evaluation(all_context_path, model)
