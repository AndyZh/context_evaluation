#!/bin/bash

# Do the eval on the contextual data and saves the predictions for the next following evaluations
./all_context_eval_scaled_yolov4.py

./size_object_eval.py --model yolov4-p5

./number_object_eval.py --model yolov4-p5

./view_point_eval.py --model yolov4-p5
