#!/usr/bin/env python3
import os
import sys
import glob
sys.path.insert(0, '..')
from context_utility_functions import get_all_datasets

DATASET_DIRECTORY = "/home/andyzhang/Bureau/Benchmark_dataset"
os.chdir(DATASET_DIRECTORY)

def get_all_models():
    model_directory = "/home/andyzhang/context_evaluation/yolov7/model_weights/trained"
    models = []
    for model in glob.glob(f"{model_directory}/*/*"):
        models.append(model)
    return models


def do_yolov7_eval_on_eval_data(list_datasets_path, model):
    """
    For all the dataset in the list_datasets_path, do an evaluation using YoloV7
    Each dataset corresponds to a context for the real data and for the synthetic data each dataset corresponds
    to a context + an intensity of the deformation of the synthetic data
    Args:
        list_datasets_path: list of path containing a dataset
    Returns:
    """
    yolov7_directory = "/home/andyzhang/context_evaluation/yolov7"
    # Add remap if necessary
    remap_files = "remap_files/sunny_day.csv"
    os.chdir(yolov7_directory)
    for dataset in list_datasets_path:
        os.system(f"python evaluation_pipeline.py --dataset {dataset} "
                  f"--weights model_weights/yolov7.pt --no-fiftyone --pred_remap {remap_files}")


def create_result_folders(models):
    result_path = "/home/andyzhang/context_evaluation/yolov7/RESULTS"
    os.system(f"rm -r {result_path}")
    os.mkdir(result_path)
    for model in models:
        experiment_name = model.split("/")[-2]
        experiment_path = f"{result_path}/{experiment_name}"
        os.mkdir(experiment_path)


def do_complete_eval(list_datasets_path, models):
    for model in models:
        os.system(f"cp {model} /home/andyzhang/context_evaluation/yolov7/model_weights/")
        do_yolov7_eval_on_eval_data(list_datasets_path, model)
        print(f"Current model : {model}")
        result_path = f"/home/andyzhang/context_evaluation/yolov7/runs/evaluation"
        new_result_path = "/home/andyzhang/context_evaluation/yolov7/RESULTS"
        experiment_name = model.split("/")[-2]
        move_result_command = f"mv {result_path}/* {new_result_path}/{experiment_name}"
        os.system(move_result_command)
        os.system("rm /home/andyzhang/context_evaluation/yolov7/model_weights/yolov7.pt")


if __name__ == "__main__":
    # Retrieve all the dataset paths
    list_datasets_path = get_all_datasets()

    models = get_all_models()
    create_result_folders(models)

    do_complete_eval(list_datasets_path, models)
