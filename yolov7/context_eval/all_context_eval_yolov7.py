#!/usr/bin/env python3
import os
import sys
sys.path.insert(0, '..')
from context_utility_functions import get_all_datasets

DATASET_DIRECTORY = "/home/andyzhang/Bureau/Benchmark_dataset"
os.chdir(DATASET_DIRECTORY)

MODEL = 'yolov7.pt'


def do_yolov7_eval_on_eval_data(list_datasets_path, model):
    """
    For all the dataset in the list_datasets_path, do an evaluation using YoloV7
    Each dataset corresponds to a context for the real data and for the synthetic data each dataset corresponds
    to a context + an intensity of the deformation of the synthetic data
    Args:
        list_datasets_path: list of path containing a dataset
    Returns:
    """
    yolov7_directory = "/home/andyzhang/context_evaluation/yolov7"
    # Add remap if necessary
    #remap_files = "remap_files/sunny_day.csv"
    os.chdir(yolov7_directory)
    for dataset in list_datasets_path:
        os.system(f"python evaluation_pipeline.py --dataset {dataset} "
                  f"--weights model_weights/{model} --no-fiftyone")


if __name__ == "__main__":
    # Retrieve all the dataset paths
    list_datasets_path = get_all_datasets()

    # Do the complete evaluation
    do_yolov7_eval_on_eval_data(list_datasets_path, MODEL)
