#!/usr/bin/env python3
import os
import sys
sys.path.insert(0, '..')
from pathlib import Path
from libia.dataset import Dataset, from_caipy, from_caipy_generic
from evaluation_pipeline import eval_with_libia
from context_utility_functions import remove_image_id, load_obj_prediction, get_all_datasets, init_eval
import shutil

DATASET_DIRECTORY = "/home/andyzhang/Bureau/Benchmark_dataset"
os.chdir(DATASET_DIRECTORY)


def filter_image_viewpoint(groundtruth, prediction, viewpoint):
    """
    This functions removes from the gt and the prediction, all the images that doest verify the viewpoint
    Args:
        groundtruth: gt dataframe
        prediction: prediction dataframe
        viewpoint: "cctv" or "front-view"
    Returns:
    """
    unique_image_ids = groundtruth.annotations['image_id'].unique()
    image_id_to_remove = []
    for image_id in unique_image_ids:
        gt_view = groundtruth.images.loc[image_id, :]['tags']['view']
        if gt_view != viewpoint:
            image_id_to_remove.append(image_id)

    groundtruth, prediction = remove_image_id(groundtruth, prediction, image_id_to_remove)

    return groundtruth, prediction


def run_view_point_evaluation(list_datasets_path, model):
    """
    Run the evaluation for all context, for images with less than 5, more than 5 and more than 30 objects
    Args:
        list_datasets_path: The list of dataset path
        model : the model we evaluate
    Returns:
    """
    for context_path in list_datasets_path:
        for number in ["front-view", "cctv"]:
            groundtruth = from_caipy(context_path).reset_index()
            groundtruth = groundtruth.remove_classes([-1])  # Remove the "ignore areas" from annotations

            predictions = load_obj_prediction(context_path, model)

            groundtruth, predictions = filter_image_viewpoint(groundtruth, predictions, number)

            # If there are only images with cctv or front-view view
            if len(groundtruth) == 0:
                continue

            yolov7_directory = "/home/andyzhang/context_evaluation/yolov7"
            os.chdir(yolov7_directory)

            context = context_path.split("/")[-1]
            output_dir = Path("runs/evaluation") / context / model / number
            if os.path.exists(output_dir):
                shutil.rmtree(output_dir)
            os.mkdir(output_dir)
            # run an evaluation with libia
            eval_with_libia(groundtruth, predictions, out_dir=output_dir)


if __name__ == "__main__":
    model = init_eval()

    # Retrieve all the dataset paths
    all_context_path = get_all_datasets()

    # Run the evaluation
    run_view_point_evaluation(all_context_path, model)
