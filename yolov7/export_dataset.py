#!/usr/bin/env python3

import fiftyone as fo

dataset = fo.load_dataset("rainy_day_evaluation")
dataset = dataset.sort_by("eval_trained_on_sunny_eval_on_rainy_day_fn", reverse=True).limit(300)

# The directory to which to write the exported dataset
export_dir = "/home/andyzhang/Bureau/rainy_day_hard"

# The name of the sample field containing the label that you wish to export
# Used when exporting labeled datasets (e.g., classification or detection)
label_field = "groundtruth"  # for example

# The type of dataset to export
# Any subclass of `fiftyone.types.Dataset` is supported
dataset_type = fo.types.COCODetectionDataset  # for example

# Export the dataset
dataset.export(
    export_dir=export_dir,
    dataset_type=dataset_type,
    label_field=label_field,
)