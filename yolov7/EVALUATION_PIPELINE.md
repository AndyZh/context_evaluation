## EVALUATION PIPELINE

This is the first version of the evaluation pipeline: a python script that aims to automate the evaluation process of our detection models.

# Current features
The current version of the evaluation pipeline provides the following features:
- Takes as input:
  - A caipyjson evaluation dataset that will serve as groundtruth
  - YOLOv7 weights that will be used to generate predictions on the evaluation dataset, or predictions that were
  already generated with a model and saved in caipyjson format.
  - An optionnal csv mapping file if you want to remap the predictions to the evaluation dataset's labelmap
- Performs an evaluation between the groundtruths and the predictions using:
  - **xxii-libia**: to compute various metrics like AP, AP0.5, AP0.3, precision, recall, f1-score.
  - **FiftyOne**: to allow you to visualise and analyse the evaluation results on the FiftyOne webapp 
  (e.g. Visualise false positives and false negatives)
- Saves the computed metrics in two JSON files:
  - `libia_eval_results.json`: Contains metrics computed using libia
  - `fiftyone_eval_results.json`: Contains metrics computed using FiftyOne

# How to use the evaluation pipeline
To use the evaluation pipeline, you need to run the script `evaluation_pipeline.py` and provide the following arguments:

**Required arguments**: 
- `--dataset`: Path to a caipyjson evaluation dataset
-  One of these two arguments:
    - `--weights`: Path to YOLOv7 weights (`.pt` file) to evaluate.
    - `--predictions`: Path to a directory containing caipyjson predictions

**Optionnal arguments**:
- `--pred_remap`: Path to a .csv file to use to remap predictions with libia. This is useful if your predictions 
labelmap is different from your evaluation dataset's labelmap
- `--img-size`: Model's input size to use when generating predictions with the provided YOLOv7 weights. `--img-size` 
is an `int`, the model's input size will be square (`--img-size`, `--img-size`), and images aspect ratio will be kept 
when resizing to this input size.
- `--conf-thres`: Confidence threshold to use when generating predictions with the provided YOLOv7 weights
- `--iou-thres`: IOU threshold for NMS to use when generating predictions with the provided YOLOv7 weights
- `--max_nms`: Maximum number of boxes into torchvision.ops.nms()
- `--device`: Device to use when generating predictions with the provided YOLOv7 weights (0 by default)
- `--no-fiftyone`: Specify this option if you are only interested in metrics computed with libia and do not wish to run 
a fiftyone evaluation which can take some time depending on the number of groundtruths and predictions.
- `--name`: Path to the directory where your predictions will be saved. If not specified, will use the name of your
weights file or caipyjson predictions folder.
- `project`: Path to the parent directory where your results will be saved. 
If not specified, will use `runs/<dataset root directory name>`

**Example:** 
```shell
python evaluation_pipeline.py --dataset /path/to/my_eval_dataset --weights /path/to/my_weights.pt
```