# Context evaluation

## Organisation de la data à évaluer

Les données d'évaluations doivent être dans un fichier appelée Benchmark_dataset, organisé de la manière suivante :

    └── Benchmark_dataset
        ├── Real                              # folder containing all the real data
            ├── data 
                ├── context_1
                    ├── Images 
                    └── Annotations 
                └── context_2
                    ...
                ...
            └── file_list                     # folder containing json files that contain the list of data
        └── Synthetic                         # folder containing all the synthetic data
            ├── data 
                ├── context_1
                    ├── intensity_0           # each set of images has different intensity of context
                        ├── Images 
                        └── Annotations 
                    ...
                    ├── intensity_n
                        ├── Images 
                        └── Annotations 
                └── context_2
                    ...
                ...
            └── file_list                      # folder containing json files that contain the list of data

## Modèles utilisés

Les modèles utilisés pour l'évaluation sont le YoloV7 et le Scaled-YoloV4 P5, tous les deux pré-entrainés sur COCO.
Les poids sont dans le fichier model_weights :

    ├── yolov7
        ├── model_weights
            ├── yolov7.pt
            └── yolov4-p5.pt 
        ...
    └── scaled_yolov4_train
## Organisation des scripts d'évaluation

Les scripts d'évaluation sont dans le fichier context_eval :

    ├── yolov7
        ├── context_eval
            ├── all_context_eval_yolov7.py        # lance les prédictions ainsi que l'évaluation sur les sous-sets contextuel à l'aide du YoloV7
            ├── all_context_eval_scaled_yolov4.py # lance les prédictions ainsi que l'évaluation sur les sous-sets contextuel à l'aide du Scaled-YoloV4 P5
            ├── context_utility_function.py       # contient un ensemble de fonctions utiles pour tout les sous-sets
            ├── number_object_eval.py             # effectue l'évaluation sur les sous-sets de nombre d'objets, il est nécessaire d'avoir effectué des prédictions au préalable avec les scripts all_context_eval 
            ├── size_object_eval.py               # meme chose que pour le script de nombre d'objets mais avec le sous-set de taille des objets
            ├── view_point_eval.py                # meme chose que pour le script de nombre d'objets mais avec le sous-set de d'angle de vue des images
            ├── do_complete_eval_yolov7.sh        # script permettant de lancer l'ensemble de la pipeline d'évaluation sur tout les sous-sets
            └── do_complete_eval_scaled_yolov4.sh # meme chose mais utilisant le scaled-yolov4 p5
        ...
    └── scaled_yolov4_train

## Lancement de l'évaluation

Afin de pouvoir lancer les scripts d'évaluation, il est nécessaire de modifier la variable globale DATASET_DIRECTORY de tous les scripts python du fichier context_eval.
Cette variable doit contenir le path vers l'emplacement du dossier Benchmark_dataset, dont l'organisation est décrite plus haut.

Il suffit alors de lancer les scripts suivant :

    ./do_complete_eval_yolov7.py         # Lance l'évaluation sur tous les sous-sets des données dans Benchmark_dataset, utilisant le YoloV7

    ./do_complete_eval_scaled_yolov4.py  # Lance l'évaluation sur tous les sous-sets des données dans Benchmark_dataset, utilisant le Scaled-YoloV4 P5

## Emplacement des résultats

Les résultats des évaluations sont dans le répertoire evaluation :

    ├── yolov7
        ├── runs
            └── evaluation

Les résultats sont alors des fichiers .json contenant l'ensemble des valeurs des métriques calculés pour les différents contextes et sous-sets.

De plus, les courbes Precision-Recall pour chaque classe et chaque évaluation sont enregistrer dans ce même répertoire.
