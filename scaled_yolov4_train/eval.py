import os
import datetime
import argparse
import json

from data import create_eval_dataset
from ScaledYOLOv4.test import test
from ScaledYOLOv4.utils.torch_utils import select_device

def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected for --save_viz (yes/no, true/false, y/n, 0/1')



def dict_to_jsonfile(dict,file_path):
    with open(file_path,"w") as file:
        json.dump(dict,file,indent=2)



def initialize_environment(params_device,batch_size):

    device = select_device(params_device,batch_size=batch_size)

    return device


def evaluation_loop(job_dir,batch_size,checkpoint,img_size,save_viz,device):
    results, maps, times =  test(
                                data= os.path.join(job_dir,"data.yaml"),
                                batch_size=batch_size,
                                imgsz=img_size,
                                weights=checkpoint,
                                save_viz= save_viz,
                                eval_device= device,
                                save_dir=job_dir
                                )

    # Write
    eval_logs = {
        "type": "evaluation",
        "precision": results[0],
        "recall": results[1],
        "map50": results[2],
        "map": results[3],
        "map_by_class": maps.tolist(),
    }
    dict_to_jsonfile(eval_logs, os.path.join(job_dir, "evaluation_results.json"))


def eval(dataset,checkpoint,batch_size,img_size,device, save_vizuals):



    """ Parameters Initialization """
    dataset_path = dataset
    save_viz = str2bool(save_vizuals)

    # output folder name will be of this structure: train_model_datetime
    job_dir = 'jobs_dir/eval_' + str(os.path.splitext(os.path.basename(checkpoint))[0]) + "_" + datetime.datetime.now().strftime(
        "%Y%m%d%H%M%S")
    os.mkdir(job_dir)


    """ Dataset creation """
    create_eval_dataset(dataset_path,job_dir)


    """ Running the evaluation loop"""
    evaluation_loop(
        job_dir= job_dir,
        batch_size = batch_size,
        checkpoint= checkpoint,
        img_size = img_size,
        save_viz = save_viz,
        device = device
    )






if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Run an evaluation job with a ScaledYOLOv4')
    parser.add_argument('--dataset', type=str, default="/data/eval_dataset",
                        help="path to the folder containing your training dataset")

    parser.add_argument('--checkpoint', type=str, default="/weights/yolov4-p6.pt",  help="path to the checkpoint if you want to finetune")
    parser.add_argument('--device', type=str, default="0", help="device(s) to use")
    parser.add_argument('--batch_size', type=str, default=1, help="evaluation batch size")
    parser.add_argument('--image_size', type=int, default=640, help="inference image size")
    parser.add_argument('--save_viz', type=str, default="no", help="save vizualisations")
    args = parser.parse_args()

    eval(dataset= args.dataset,
         checkpoint=args.checkpoint,
         batch_size=args.batch_size,
         img_size=args.image_size,
         device=args.device,
         save_vizuals=args.save_viz)
