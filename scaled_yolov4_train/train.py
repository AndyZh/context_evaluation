
import data
import os
import argparse
import json
from ScaledYOLOv4.train import train as train_scaledyolov4
from ScaledYOLOv4.utils.torch_utils import select_device
import datetime
import yaml
from torch.utils.tensorboard import SummaryWriter


def train_params_initialization():

    """ Initializes different parameters that should be fed to the scaled yolov4 training loop.
    Some parameters are not relevant to our usecase like the DDP (Distributed Data Parallel) parameters but should be initialized
    in order to not cause any crash.
    """

    #parser = argparse.ArgumentParser()
    parser.add_argument('--weights', type=str, default='yolov4-p5.pt', help='initial weights path')
    parser.add_argument('--cfg', type=str, default='', help='model.yaml path')
    parser.add_argument('--data', type=str, default='data/coco128.yaml', help='data.yaml path')
    parser.add_argument('--hyp', type=str, default='', help='hyperparameters path, i.e. data/hyp.scratch.yaml')
    parser.add_argument('--epochs', type=int, default=300)
    parser.add_argument('--batch-size', type=int, default=16, help='total batch size for all GPUs')
    parser.add_argument('--img-size', nargs='+', type=int, default=[640, 640], help='train,test sizes')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--adam', action='store_true', help='use torch.optim.Adam() optimizer')
    parser.add_argument('--logdir', type=str, default='runs/', help='logging directory')
    parser.add_argument('--save_checkpoint_every_n', type=int, default=10, help='save checkpoints every n steps')

    parser.add_argument('--resume', nargs='?', const='get_last', default=False,
                        help='resume from given path/last.pt, or most recent run if blank')
    parser.add_argument('--rect', action='store_true', help='rectangular training')
    parser.add_argument('--multi-scale', action='store_true', help='vary img-size +/- 50%%')
    parser.add_argument('--cache-images', action='store_true', help='cache images for faster training')
    parser.add_argument('--evolve', action='store_true', help='evolve hyperparameters')
    parser.add_argument('--nosave', action='store_true', help='only save final checkpoint')
    parser.add_argument('--notest', action='store_true', help='only test final epoch')
    parser.add_argument('--noautoanchor', action='store_true', help='disable autoanchor check')
    parser.add_argument('--bucket', type=str, default='', help='gsutil bucket')
    parser.add_argument('--name', default='', help='renames results.txt to results_name.txt if supplied')
    parser.add_argument('--single-cls', action='store_true', help='train as single-class dataset')
    parser.add_argument('--sync-bn', action='store_true', help='use SyncBatchNorm, only available in DDP mode')
    parser.add_argument('--local_rank', type=int, default=-1, help='DDP parameter, do not modify')

    params = parser.parse_args()
    params.total_batch_size = params.batch_size
    params.global_rank = -1
    params.world_size = 1

    return params



def initialize_environment(params_device,batch_size):

    device = select_device(params_device,batch_size=batch_size)

    return device



def update_training_parameters(user_params,job_dir, job_name, config,checkpoint):

    """ Update the parameters train_params_initialization() with the parameters defines by the user
    in training_parameters.json """

    #Initializing the parameters
    params = train_params_initialization()

    #If we finetune, we import the recommanded hyperparameters from ScaledYOLOv4/data/hyp.finetun.yaml,
    #If we train from scratch, we import the hyperparameters from hyp.scratch.yaml
    params.hyp = params.hyp or ('ScaledYOLOv4/data/hyp.finetune.yaml' if params.weights else 'data/hyp.scratch.yaml')
    with open(params.hyp) as file:
        hyperparams = yaml.load(file, Loader=yaml.FullLoader)

    #Updating the parameters and hyperparameters
    params.epochs = user_params["epochs"]
    params.batch_size = user_params["batch_size"]
    params.img_size = user_params["img_size"]
    params.img_size.extend([params.img_size[-1]] * (2 - len(params.img_size)))  # extend to 2 sizes (train, test)
    params.device = user_params["device"]
    # if adam is set to true, we use adam optimizer, else we use SGD optimizer
    params.adam = user_params["adam"]
    params.logdir = job_dir
    params.name = job_name
    params.cfg = config
    params.weights = checkpoint
    #the data.yaml will be created by create_train_dataset() and stored in jobdir/data.yaml
    params.data = os.path.join(job_dir,"data.yaml")
    params.save_checkpoint_every_n = user_params["save_checkpoint_every_n"]
    params.max_checkpoints_to_keep = user_params["max_checkpoints_to_keep"]
    params.save_metrics_every_n = user_params["save_metrics_every_n"]

    hyperparams["lr0"] = user_params["learning_rate"]
    hyperparams["momentum"] = user_params["momentum"]
    hyperparams["weight_decay"] = user_params["weight_decay"]

    return hyperparams, params



def training_loop(hyperparams, params, device, labelmap):

    tb_writer = SummaryWriter(log_dir=params.logdir)
    train_scaledyolov4(hyperparams, params, device, labelmap, tb_writer)




def train(dataset,model_config,checkpoint,valid_size,job_name):

    """ Parameters Initialization """
    dataset_path = dataset
    checkpoint_path = checkpoint
    config_path = model_config

    # output folder name will be of this structure: train_model_datetime
    job_dir = 'jobs_dir/train_' + str(os.path.splitext(os.path.basename(config_path))[0]) + "_" + job_name + "_"\
              + datetime.datetime.now().strftime(
        "%Y_%m_%d_%H_%M_%S")
    if not os.path.exists("jobs_dir"): os.mkdir("jobs_dir")
    os.mkdir(job_dir)


    """ Dataset creation """
    labelmap = data.create_train_dataset(dataset_path,job_dir,float(valid_size))


    """ Reading the training hyperparameters """
    with open("training_parameters.yaml", "r") as file:
        user_params = yaml.safe_load(file)

    """ Updating the training parameters """
    hyperparams, params = update_training_parameters(user_params=user_params,
                                                     job_dir=job_dir,
                                                     job_name= job_name,
                                                     config=config_path,
                                                     checkpoint=checkpoint_path)

    """ Initialize the device(s)"""
    device = initialize_environment(params_device=params.device,batch_size=params.batch_size)


    """ Running the training loop"""
    training_loop(hyperparams,params,device,labelmap)







if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Run a training job with a ScaledYOLOv4', prog='train.py')

    parser.add_argument('--dataset', type=str, default="/data/train_dataset",
                        help="path to the folder containing your training dataset")

    parser.add_argument('--config', type=str, default="/scaledyolov4/models/yolov4-p6.yaml",
                        help="path to the config.py of your model inside /scaledyolov4/models/")
    parser.add_argument('--checkpoint', type=str, default="",
                        help="path to the checkpoint if you want to finetune")
    parser.add_argument('--valid_size', type=str, default=0.2,
                        help="size of the validation set if your dataset is not presplitted (a randomsplit will be performed")
    parser.add_argument('--job_name', type=str, default="",
                        help="Your job's name")
    args = parser.parse_args()

    train(dataset= args.dataset,
          checkpoint=args.checkpoint,
          model_config=args.config,
          valid_size=args.valid_size,
          job_name=args.job_name)

