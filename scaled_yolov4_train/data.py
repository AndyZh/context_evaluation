import json
import os
import yaml
from sklearn.model_selection import train_test_split



def coco_bbox_to_yolo_bbox(coco_bbox,img_height,img_width):
    """
    Converts a coco bbox (x_top-left, y_top-left, width, height)
    to a yolo bbox (x_center_relative, y_center_relative, width_relative, height_relative)

    (Relative means that the value is divided by the absolute height or width of the image and is between 0-1)

    Args:
        coco_bbox: a 4 elements array containing a coco_bbox
        img_height: Absolute height of the image
        img_width: Absolute width of the image
    """
    x_top_left = coco_bbox[0]
    y_top_left = coco_bbox[1]
    bbox_width = coco_bbox[2]
    bbox_height = coco_bbox[3]

    x_center_relative = (x_top_left + bbox_width/2) / img_width
    y_center_relative = (y_top_left + bbox_height/2) / img_height
    width_relative = bbox_width / img_width
    height_relative = bbox_height / img_height

    yolo_bbox = [x_center_relative,y_center_relative,width_relative,height_relative]

    return yolo_bbox


def caipyjson_to_yolo(annotations_path,images_path,job_dir):

    """ Converts caipyjson annotations to yolo .txt annotations

    Args:
        annotations_path: path to the annotations folder
        images_path: path to the images folder (where the annotation.txt files should be saved)
        job_dir: path to the training/evaluation job directory

    Returns:
          sorted_labelmap: dictionnary sorted by id of the different class names/id

        """


    label_map = {}

    # load annotation.json files
    for file_name in [file for file in os.listdir(annotations_path) if file.endswith('.json')]:
        with open(os.path.join(annotations_path, file_name)) as json_file:


            #Reading the data from the current json file
            data = json.load(json_file)
            img_name =  data["image"]["file_name"]
            img_height =  data["image"]["height"]
            img_width =   data["image"]["width"]

            #Creating the annotation.txt file that should have the same name as the image
            file_path = os.path.join(images_path,
                                     os.path.splitext(img_name)[0] + ".txt") #we remove .jpg and add .txt to img_name
            ann_txt_file = open(file_path, "w+")

            #Processing every object annotation in the image
            for ann in data["annotations"]:

                #converting the bbox format from coco to yolo
                yolo_bbox = coco_bbox_to_yolo_bbox(ann["bbox"] , img_height , img_width)
                category_id = ann["category_id"]

                #Writing each object annotation in a line of the .txt file
                line = str(category_id) + " " + str(yolo_bbox[0]) + " " + str(yolo_bbox[1]) + " " + str(yolo_bbox[2]) + " "  + str(yolo_bbox[3]) + "\n"
                ann_txt_file.write(line)

                #Adding the category to the labelmap if it is not already in
                label_map[ann["category_id"]] = ann["category_str"]

            #closing the current .txt file
            ann_txt_file.close()


    # Sorting the labelmap dictionnary by id
    sorted_labelmap = {}
    for key in sorted(label_map.keys()):
        sorted_labelmap[key] = label_map[key]

    # Saving labelmap a json file
    with open(os.path.join(job_dir, 'label_map.json'), 'w') as file:
        json.dump(sorted_labelmap, file, indent=4)

    return sorted_labelmap


def has_discontinuities(label_map):
    """ This function checks for discontinuities in the labelmap's keys (class ID) and returns a new labelmap with no discontinuities
    Discontinuities in the labelmap are not supported by the scaled yolov4 implementation.
    Therefore, we remove them by creating a new labelmap with no discontinuities

    Args:
        label_map: the original labelmap of the dataset

    Returns:
        Boolean: if there are discontinuities or not
        new_keys: a dictionnary mapping between the original discontinuous keys (class ID) and the new continuous keys   """

    new_keys = {}
    new_key = 0

    for key in label_map.keys():
        new_keys[key] = new_key
        new_key = new_key + 1

    discontinuous = False

    original_ids = list(label_map.keys())
    discontinuous_ids = list(new_keys.values() )

    for i in range( len(original_ids) ):
        if original_ids[i] != discontinuous_ids[i] : discontinuous = True


    return discontinuous , new_keys




def fix_labelmap_discontinuities(annotations_path,new_keys,label_map,job_dir):
    """ Function called when the labelmap keys are discontinuous. It updates the annotations.txt with the new
    classes ID

    Args:
       annotations_path: path to the .txt annotations
       new_keys: a dictionnary mapping between the original discontinuous keys (class ID) and the new continuous keys
       label_map: the current discontinuous label_map
       job_dir: the current job's dir

    Returns:
       new_label_map: the new continuous labelmap    """


    for file_name in [file for file in os.listdir(annotations_path) if file.endswith(".txt") ] :

            #Reading the current lines of the annotation.txt file
            ann_file = open( os.path.join(annotations_path,file_name), "r")
            lines = ann_file.readlines()
            new_lines = []

            #for each line in the annotation.txt file we change the class_id according to new_keys
            for line in lines:
                #splitting the line into 5 elements (class_id, x_center,y_center,width,height)
                class_id,x_center,y_center,w,h = line.split()

                #generating the new_class_id from the new_keys dictionary
                new_class_id = new_keys[int(class_id)]

                #Creating the new line
                new_line = str(new_class_id) + " " + x_center + " " + y_center + " " + w + " " +  h + "\n"
                new_lines.append(new_line)

            #Writing the new lines in the .txt file
            ann_file = open( os.path.join(annotations_path,file_name), "w")
            ann_file.writelines(new_lines)
            ann_file.close()


    #Creating the new continuous label map
    new_label_map = {}
    for key in label_map.keys():
        #the key of the new_label_map is new_keys[key]
        #the value of the new_label_map is label_map[key]
        new_label_map[new_keys[key]] = label_map[key]

    #saving the new_label_map in the job's dir
    with open(os.path.join(job_dir, 'label_map.json'), 'w') as file:
        json.dump(new_label_map, file, indent=4)

    return new_label_map





def create_image_list_txt(folder,output):
    """ Creates a .txt file containing the list of all the .jpeg images in a specific folder """


    output_file = open(output, "w+")
    for file_name in [file for file in os.listdir(folder) if file.endswith('.jpg') or file.endswith('.jpeg')]:
        file_path = os.path.join(folder,file_name)
        output_file.write(str(file_path) + "\n")

    output_file.close()


def create_randomsplit_image_list_txt(valid_size,images_path,train_file,valid_file):

    """ Randomly splits the unsplitten dataset images and saves the train images paths in a train.txt file
    and valid images list in a valid.txt file. The split is performed according to a validation size ratio between 0-1

       Args:
           valid_size: ratio of the size of the validation subset (between 0-1)
           job_dir: the job's directory
           images_path: path to the folder containing the images
           train_file : path to the train.txt to output
           valid_file: path to the val.txt to output

           """

    # List of all the dataset's images
    img_list = [file for file in os.listdir(images_path) if file.endswith('.jpg') or file.endswith('.jpeg')]
    # Randomy splitting the image list into a train and valid sets
    train_img_list, valid_img_list = train_test_split(img_list, test_size=valid_size, shuffle=True)

    # create the two .txt files
    train_txt_file = open(train_file,"w+")
    valid_txt_file = open(valid_file, "w+")

    #Creating the train images list
    for img_name in train_img_list:
        file_path = os.path.join(images_path,img_name)
        train_txt_file.write(str(file_path) + "\n")

    # Creating the valid images list
    for img_name in valid_img_list:
        file_path = os.path.join(images_path, img_name)
        valid_txt_file.write(str(file_path) + "\n")

    #Closing the files
    train_txt_file.close()
    valid_txt_file.close()




def create_data_yaml(job_dir, labelmap, train_txt_path = "", val_txt_path = "",test_txt_path = ""):
    """ creates the data.yaml file that contains:

        train.txt: individual paths to the training images
        valid.txt: individual paths to the validation images
        nc: number of classes
        names[]: class names

    Args:
        job_dir: the job's directory
        labelmap: the dataset's labelmap
        train_txt_path : path to the train.txt
        val_txt_path: path to the val.txt

        """

    with open( os.path.join(job_dir,"data.yaml"), "w") as file:
        #creating the file content
        content = {
            "train" : train_txt_path ,
            "val" : val_txt_path ,
            "test" : test_txt_path,
            "nc" : len(labelmap.values()),
            "names" : [str(name) for name in labelmap.values()]
        }

        #Writing the file content
        yaml.dump(content,file)





def create_train_dataset(dataset_path,job_dir,valid_size=0.2):

    """ Creates from a dataset in the caipyjson format, the necessary files to train a scaledyolov4:
              - yolo.txt annotations
              - train.txt and val.txt containing the paths to the train/val images
              - data.yaml file containing the paths to train.txt, val.txt, class number, and class names


              """

    # If the dataset is pre-splitted
    if os.path.exists(os.path.join(dataset_path,"Annotations/valid")):

        label_map = create_pre_splitted_dataset(dataset_path,job_dir)

    # Else we randomly split the dataset using a valid_size ratio parameter
    else:
        label_map = create_random_splitted_dataset(dataset_path,job_dir,valid_size)



    return label_map


def create_pre_splitted_dataset(dataset_path,job_dir):

    """ Creates from a caiyjson dataset all the necessary files to train a scaledyolov4:
        - Annotations files
        - train.txt and val.txt files containing the individual paths to every image
        - data.yaml containing the paths to train.txt, val.txt, the number of classes and the classes names

        Args:
            dataset_path: path to the root of the dataset
            job_dir: path to the current training job's directory

        Returns:
              labelmap: a dictionnary of the dataset's labelmap

            """
    #initializing the paths
    train_annotations_path = os.path.join(dataset_path,"Annotations/train")
    train_images_path = os.path.join(dataset_path,"Images/train")
    valid_annotations_path = os.path.join(dataset_path,"Annotations/valid")
    valid_images_path = os.path.join(dataset_path,"Images/valid")

    # remove yolo cache files if they exist. They are generated under dataset/
    # when evaluating a scaledyolo to accelerate the evaluation
    for cache_file_name in [file for file in os.listdir(os.path.join(dataset_path,"Images/")) if
                            file.endswith('.cache')]:
        os.remove(os.path.join(dataset_path,"Images/", cache_file_name))


    #Converting caipyjson annotations to yolo annotations
    label_map_train = caipyjson_to_yolo(annotations_path=train_annotations_path,
                                        images_path=train_images_path,
                                        job_dir=job_dir
                                        )

    label_map_valid = caipyjson_to_yolo(annotations_path= valid_annotations_path,
                                        images_path= valid_images_path,
                                        job_dir= job_dir
                                        )

    print("training label_map :",label_map_train)
    print("validation label_map :", label_map_valid)
    assert label_map_train == label_map_valid , "Your training labelmap is different from your validation labelmap"


    """ Checking if the labelmap's classes ID are discontinuous. If true, we need to remove the discontinuities because
     the scaled yolov4 training script does not support them. 
     Example: if our dataset has 3 classes, the classes ID should be 0,1,2 and not 0,1,3 """
    discontinuous, new_keys = has_discontinuities(label_map_train)

    if discontinuous :
       print("The original training labelmap is discontinuous: ", label_map_train)
       #fix training annotations
       label_map_train =  fix_labelmap_discontinuities(
                                     annotations_path=train_images_path,
                                     label_map=label_map_train,
                                     new_keys=new_keys,
                                     job_dir=job_dir )
       #fix validation annotations
       label_map_valid = fix_labelmap_discontinuities(
                                      annotations_path=valid_images_path,
                                      label_map=label_map_valid,
                                      new_keys=new_keys,
                                      job_dir=job_dir )

       print("The discontinuities have been fixed on the training labelmap: ", label_map_train)
       print("The discontinuities have been fixed on the validation labelmap: ", label_map_valid)

    #Creating train.txt and val.txt (text files containing the training and valiation image paths)

    train_txt_path = os.path.join(job_dir,"train.txt")
    val_txt_path = os.path.join(job_dir,"val.txt")

    create_image_list_txt(train_images_path,train_txt_path)
    create_image_list_txt(valid_images_path,val_txt_path)

    #Create the data.yaml file
    create_data_yaml(job_dir,label_map_train,train_txt_path,val_txt_path)


    return label_map_train





def create_random_splitted_dataset(dataset_path,job_dir,valid_size):

    """ Creates from an unsplitted caiyjson dataset all the necessary files to train a scaledyolov4:
            - Annotations files
            - train.txt and val.txt files containing the individual paths to every image
            - data.yaml containing the paths to train.txt, val.txt, the number of classes and the classes names

            Args:
                dataset_path: path to the root of the dataset
                job_dir: path to the current training job's directory
                valid_size:  size of the random validation datasubset that will be created (between 0-1. Ex: 0.3)

            Returns:
                  labelmap: a dictionnary of the dataset's labelmap

                """

    # Initializing paths
    annotations_path = os.path.join(dataset_path, "Annotations")
    images_path = os.path.join(dataset_path, "Images")

    # remove yolo cache files if they exist. They are generated under dataset/
    # when evaluating a scaledyolo to accelerate the evaluation
    for cache_file_name in [file for file in os.listdir(dataset_path) if
                            file.endswith('.cache')]:
        os.remove(os.path.join(dataset_path, cache_file_name))


    # Converting caipyjson annotations to yolo annotations
    label_map = caipyjson_to_yolo(annotations_path=annotations_path,
                                  images_path=images_path,
                                  job_dir=job_dir
                                  )

    """ Checking if the labelmap's classes ID are discontinuous. If true, we need to remove the discontinuities because
         the scaled yolov4 does not support them. 
         Example: if our dataset has 3 classes, the classes ID should be 0,1,2 and not 0,1,3 """

    discontinuous, new_keys = has_discontinuities(label_map)
    if discontinuous:
        print("The original labelmap is discontinuous: ", label_map)
        # fix annotations
        label_map = fix_labelmap_discontinuities(
            annotations_path=images_path,
            label_map=label_map,
            new_keys=new_keys,
            job_dir=job_dir)


    # Creating train.txt and val.txt (text files containing the training and valiation image paths)
    # The files will be saved in the root of the dataset path)

    train_txt_path = os.path.join(job_dir, "train.txt")
    val_txt_path = os.path.join(job_dir, "val.txt")

    # Randomly splitting the image list into a train and valid subset
    # and creating train.txt and val.txt (text files containing the training and valiation image paths)
    create_randomsplit_image_list_txt(valid_size,images_path,train_txt_path,val_txt_path)

    # Create the data.yaml file
    create_data_yaml(job_dir, label_map, train_txt_path, val_txt_path)

    return label_map


def create_eval_dataset(dataset_path,job_dir):
    """ Creates from a caiyjson dataset all the necessary files to evaluate a scaledyolov4:
           - Annotations files
           - test.txt file containing the individual paths to every image
           - data.yaml containing the paths to test.txt, the number of classes and the classes names

           Args:
               dataset_path: path to the root of the dataset
               job_dir: path to the current training job's directory

           Returns:
                 labelmap: a dictionnary of the dataset's labelmap

               """

    #Initializing paths
    annotations_path = os.path.join(dataset_path, "Annotations")
    images_path = os.path.join(dataset_path, "Images")

    # remove yolo cache files if they exist. They are generated under dataset/
    # when evaluating a scaledyolo to accelerate the evaluation
    for cache_file_name in [file for file in os.listdir(dataset_path) if
                            file.endswith('.cache')]:
        os.remove(os.path.join(dataset_path, cache_file_name))

    #Converting caipyjson annotations to yolo annotations
    label_map = caipyjson_to_yolo(annotations_path=annotations_path,
                                        images_path=images_path,
                                        job_dir=job_dir
                                        )



    """ Checking if the labelmap's classes ID are discontinuous. If true, we need to remove the discontinuities because
     the scaled yolov4 does not support them. 
     Example: if our dataset has 3 classes, the classes ID should be 0,1,2 and not 0,1,3 """

    discontinuous, new_keys = has_discontinuities(label_map)
    if discontinuous:
        print("The original labelmap is discontinuous: ", label_map)
        # fix annotations
        label_map = fix_labelmap_discontinuities(
            annotations_path=images_path,
            label_map=label_map,
            new_keys=new_keys,
            job_dir=job_dir)
        print("The discontinuities have been fixed ", label_map)

    # Creating test.txt (text files containing the evaluation images paths)
    # The file will be saved in the root of the dataset path)

    test_txt_path = os.path.join(job_dir, "test.txt")

    create_image_list_txt(images_path, test_txt_path)

    # Create the data.yaml file
    create_data_yaml(job_dir = job_dir,
                     labelmap = label_map,
                     test_txt_path = test_txt_path)
