# Running a training and evaluation job with the Scaled YOLOv4


You can use this project to run a training and/or evaluation job using a scaled YOLOv4 from https://github.com/WongKinYiu/ScaledYOLOv4

To use your dataset with this project, it must be in the caipyjson format.

## Installation

First, you need to clone this project on your local machine:

Then, you need to build the docker image by opening a terminal in the root of the cloned repository and running the following command: 

`sh docker_build`


## Usage

Run the docker image that you built by running the following command:

`sudo ./run_docker.sh <container name> <dataset(s) path> <weights path>`

This command takes 3 positional arguments:
- container name: A name for the container to be created that will help you distinguish it from other 
running containers. Ex: my_training_1
- dataset(s) path: Local path to your dataset(s) that will be mounted in the container at `/data`
- weights path: local path to your weights that will be mounted in the container at `/weights`

Full example: `sudo ./run_docker.sh my_training_1 /media/Data2/my_datasets /media/Data2/my_weights`

### Training


You can directly run a training with the provided model and the provided dataset by running the following command:

`python3 train.py`

To customize your training parameters, you can edit training_parameters.yaml

If you want to specify another model or dataset, you can pass the following arguments to the train.py script:

- --config : Path to the training model config inside /scaledyolov4/models. Ex: --config /scaledyolov4/models/yolov4-p6.yaml
- --dataset : Path to the training dataset. Ex: --dataset /data/train_dataset
- -- checkpoint (Optional): Path to the checkpoint.pth if you want to finetune. Ex: --checkpoint /weights/yolov4-p6.pt. To train from scratch leave this argument empty.
- --valid_size: If your dataset is not pre-splitted, it is randomly splitted into train/val subsets. you can set the ratio size of the valid set (between 0-1) with this argument. Ex: --valid_size 0.2 to use 20% of your data for the validation process.


When running a training job, the parameters and hyperparameters, weights, labelmap, and training logs will be exported to a directory that will be created under jobs_dir/. 
The directory's name will be of this structure: train_modelname_datetime

### Evaluation


You can directly run an evaluation with the provided model and the provided dataset by running the following command:

`python3 eval.py`

If you want to specify another model, dataset, or some basic evaluation parameters, you can pass the following arguments to the eval.py script:

- --config :path to the model config inside /scaledyolov4/models. Ex: --config /scaledyolov4/models/yolov4-p6.yaml
- --dataset : Path to the evaluation dataset.  Ex: --dataset /data/eval_dataset
- --checkpoint : Path to the checkpoint.pth to evaluate. Ex: --checkpoint /weights/yolov4-p6.pt
- --save_viz: Save groundtrouth and prediction inside a visualizations/ folder. Ex: --save_viz yes
- --image_size: Change the inference image size  Ex: --image_size 640

When running an evaluation job, the labelmap, the vizualisations and the evaluation logs will be outputed to a directory that will be created under jobs_dir/. 
The directory name will be of this structure: eval_modelname_datetime

**Note:**

In order to use your dataset for a training or evaluation job, the annotations must be in caipy.json format. 

Conversion scripts can be found at the repository https://bitbucket.org/xxii-io/caipyjson_conversions/src/master/ to convert some of the most used annotation formats to caipyjson (ex: coco_to_caipyjson.py)




