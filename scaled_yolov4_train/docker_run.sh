#! /bin/bash

docker run --gpus='"device=1"' --name $1 -it --shm-size=64g \
  -v $2:/data \
  -v $3:/weights \
  -v $(pwd):/workdir \
  scaledyolov4:latest /bin/bash

